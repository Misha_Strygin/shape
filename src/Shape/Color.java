package Shape;

public enum Color {
    red,
    blue,
    white,
    black,
    green,
    yellow
}